create database HRM;
use HRM;

CREATE TABLE `user` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `fullname` varchar(100) NOT NULL,
 `dob` varchar(100) NOT NULL,
 `address` varchar(200) NOT NULL,
 `phoneNumber` varchar(20) NOT NULL,
 `email` varchar(255) NOT NULL,
 `password` varchar(60) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `role` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(255) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user_role` (
 `user_id` int(11) NOT NULL,
 `role_id` int(11) NOT NULL,
 PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

