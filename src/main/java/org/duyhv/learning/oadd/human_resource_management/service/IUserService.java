package org.duyhv.learning.oadd.human_resource_management.service;

import org.duyhv.learning.oadd.human_resource_management.domain.User;

import java.util.List;

public abstract interface IUserService {
    User findByEmail(String email);

    User findByEmailAndPassword(String username, String password);
    List<User> findUsersByRoleName(String roleName);
}