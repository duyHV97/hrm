package org.duyhv.learning.oadd.human_resource_management.service.impl;

import org.duyhv.learning.oadd.human_resource_management.domain.User;
import org.duyhv.learning.oadd.human_resource_management.repository.IUserRepository;
import org.duyhv.learning.oadd.human_resource_management.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService implements IUserService {

    @Autowired
    private IUserRepository iUserRepository;

    @Override
    public User findByEmail(String email) {
        return this.iUserRepository.findUserByEmail(email);
    }

    @Override
    public User findByEmailAndPassword(String username, String password) {
        return this.iUserRepository.findUserByEmailAndPassword(username, password);
    }

    @Override
    public List<User> findUsersByRoleName(String roleName) {
        return this.iUserRepository.findUsersByRoleName(roleName);
    }
}