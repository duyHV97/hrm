package org.duyhv.learning.oadd.human_resource_management.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@Table(name = "user")
@RequiredArgsConstructor
public class User implements Serializable {
    /**
     * Serializable help us transform User object into new object form for using in
     * another processes, in general, this object is transformed into byte streams
     * and we can save that byte stream data in storage, hard storage, or transport
     * to a server or we can save into our server.
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    @Column(name = "fullname", nullable = false)
    private String fullname;

    @Column(name = "dob", nullable = false)
    private String dob;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "phoneNumber", nullable = false)
    private String phoneNumber;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "password", nullable = false)
    private String password;

    // @JoinTable define components that take part in the @ManyToMany reference
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id")
    @JsonBackReference
    private Role role;
}