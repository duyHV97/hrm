package org.duyhv.learning.oadd.human_resource_management.service;

import org.duyhv.learning.oadd.human_resource_management.domain.Role;

import java.util.List;

public interface IRoleService {

    List<Role> findByName(String name);
}
