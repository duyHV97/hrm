package org.duyhv.learning.oadd.human_resource_management.repository;

import java.util.List;

import org.duyhv.learning.oadd.human_resource_management.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserRepository extends JpaRepository<User, Integer> {
    User findUserByEmail(String email);

    User findUserByEmailAndPassword(String username, String password);

    // select * from user, role where user.role_id= role.id and role.name= 'qlkv'
    @Query(value = "SELECT distinct * from user u, role r where u.role_id= r.id and r.name= ?1", nativeQuery = true)
    List<User> findUsersByRoleName(String roleName);
}