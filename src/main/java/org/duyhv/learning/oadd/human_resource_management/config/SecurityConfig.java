package org.duyhv.learning.oadd.human_resource_management.config;

import java.util.List;

import org.duyhv.learning.oadd.human_resource_management.domain.User;
import org.duyhv.learning.oadd.human_resource_management.service.impl.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    private final UserService userService;

    public SecurityConfig(UserService userService) {
        this.userService = userService;
    }

    // findUsersByRoleName= tql
    // @Bean
    public List<User> findUsersByRoleTql() {
        List<User> tqls = this.userService.findUsersByRoleName("tql");
        log.info("So tong quan ly= " + tqls.size() + " quan ly dau tien la: " + tqls.get(0).getFullname());
        return tqls;
    }

    // findUsersByRoleName= qlkv
    // @Bean
    List<User> findUsersByRoleQlkv() {
        return this.userService.findUsersByRoleName("qlkv");
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        List<User> qlkvs = this.findUsersByRoleQlkv();
        List<User> tqls = this.findUsersByRoleTql();
        if (!qlkvs.isEmpty()) {
            qlkvs.forEach(qlkv -> {
                try {
                    auth.inMemoryAuthentication().withUser(qlkv.getEmail())
                            .password(this.passwordEncoder().encode(qlkv.getPassword())).roles("qlkv").and();
                    log.info(qlkv.getEmail() + " with " + qlkv.getRole().getName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
        if (!tqls.isEmpty()) {
            tqls.forEach(tql -> {
                try {
                    auth.inMemoryAuthentication().withUser(tql.getEmail())
                            .password(this.passwordEncoder().encode(tql.getPassword())).roles("tql").and();
                    log.info(tql.getEmail() + " with " + tql.getRole().getName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
        auth.inMemoryAuthentication().withUser("hoangduytd97").password(this.passwordEncoder().encode("hoangduytd97"))
                .roles("ADMIN");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic().and().exceptionHandling()
                .authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED)).and().authorizeRequests()
                // .antMatchers(HttpMethod.POST, "/login").authenticated()
                .antMatchers(HttpMethod.GET, "user/**").hasRole("qlkv").antMatchers(HttpMethod.PUT, "user/**")
                .hasRole("qlkv").antMatchers(HttpMethod.POST, "user/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "user/**").hasRole("qlkv").and().formLogin().disable();
    }

}