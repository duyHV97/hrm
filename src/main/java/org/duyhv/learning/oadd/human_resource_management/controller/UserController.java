package org.duyhv.learning.oadd.human_resource_management.controller;

import java.util.List;

import org.duyhv.learning.oadd.human_resource_management.domain.User;
import org.duyhv.learning.oadd.human_resource_management.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/findByUsernameAndPassword")
    public ResponseEntity<User> findByUsernameAndPassword(
            @RequestParam(value = "username", required = true, defaultValue = " ") String username,
            @RequestParam(value = "password", required = true, defaultValue = " ") String password) {
        User user = this.userService.findByEmailAndPassword(username, password);
        if (user == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(user);
    }

    @GetMapping(value = "/findByEmail")
    public ResponseEntity<User> findByEmail(@RequestParam(value = "email") String email) {
        User user = this.userService.findByEmail(email);
        if (user == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(user);
    }

    @GetMapping(value = "/findByRoleName")
    public ResponseEntity<List<User>> findByRoleName(@RequestParam(value = "roleName") String roleName) {
        List<User> users = this.userService.findUsersByRoleName(roleName);

        if (!users.isEmpty()) {
            return ResponseEntity.ok().body(users);
        }
        return ResponseEntity.notFound().build();
    }
}
