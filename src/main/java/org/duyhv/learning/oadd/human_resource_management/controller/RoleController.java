package org.duyhv.learning.oadd.human_resource_management.controller;

import java.util.List;

import org.duyhv.learning.oadd.human_resource_management.domain.Role;
import org.duyhv.learning.oadd.human_resource_management.service.impl.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/role")
public class RoleController {

    private RoleService roleService;

    @Autowired
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @GetMapping(value = "/findByName")
    public ResponseEntity<List<Role>> findByName(@RequestParam(value = "name") String name) {
        List<Role> roles = this.roleService.findByName(name);
        if (!roles.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(HttpStatus.FOUND).build();
    }
}