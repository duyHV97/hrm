package org.duyhv.learning.oadd.human_resource_management.repository;

import org.duyhv.learning.oadd.human_resource_management.domain.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IRoleRepository extends CrudRepository<Role, Integer> {
    List<Role> findRolesByName(String name);
}