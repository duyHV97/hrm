package org.duyhv.learning.oadd.human_resource_management.controller;

import org.duyhv.learning.oadd.human_resource_management.domain.User;
import org.duyhv.learning.oadd.human_resource_management.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {

    private UserService userService;

    @Autowired
    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(value = "/login")
    public ResponseEntity<User> login(@RequestBody(required = true) String email,
            @RequestBody(required = true) String password, HttpRequest request) {
        User user = this.userService.findByEmailAndPassword(email, password);
        return ResponseEntity.ok(user);
    }
}