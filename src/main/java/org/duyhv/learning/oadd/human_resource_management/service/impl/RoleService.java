package org.duyhv.learning.oadd.human_resource_management.service.impl;

import org.duyhv.learning.oadd.human_resource_management.domain.Role;
import org.duyhv.learning.oadd.human_resource_management.repository.IRoleRepository;
import org.duyhv.learning.oadd.human_resource_management.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleService implements IRoleService {

    private IRoleRepository iRoleRepository;

    @Autowired
    public RoleService(IRoleRepository iRoleRepository) {
        this.iRoleRepository = iRoleRepository;
    }

    @Override
    public List<Role> findByName(String name) {
        return this.iRoleRepository.findRolesByName(name);
    }
}
